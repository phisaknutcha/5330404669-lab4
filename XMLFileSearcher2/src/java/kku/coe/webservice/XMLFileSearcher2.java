package kku.coe.webservice;

import java.io.*;
import java.util.Scanner;
import javax.xml.stream.*;
import javax.xml.stream.events.*;

public class XMLFileSearcher2 {

    public static void main(String args[]) throws FileNotFoundException, XMLStreamException {

        String keyword = null;
        String eName = null;
        String by = null;
        String words = null;
        String kwFile = "keyword.txt";
        File xmlFile = new File("quotes.xml");
        boolean quoteFound = false;
        boolean wordsFound = false;
        boolean byFound = false;
        Scanner scanner = new Scanner(new FileInputStream(kwFile), "UTF-8");

        try {
            while (scanner.hasNextLine()) {
                keyword = scanner.nextLine();

                XMLInputFactory factory = XMLInputFactory.newInstance();
                XMLEventReader reader = factory.createXMLEventReader(new InputStreamReader(new FileInputStream(xmlFile)));


                while (reader.hasNext()) {

                    XMLEvent event = reader.nextEvent();

                    if (event.isStartElement()) {
                        StartElement element = (StartElement) event;
                        eName = element.getName().getLocalPart();
                        if (eName.equals("quote")) {
                            quoteFound = true;
                        }
                        if (quoteFound && eName.equals("words")) {
                            wordsFound = true;
                        }
                        if (quoteFound && eName.equals("by")) {
                            byFound = true;
                        }
                    }

                    if (event.isEndElement()) {
                        EndElement element = (EndElement) event;
                        eName = element.getName().getLocalPart();
                        if (eName.equals("quote")) {
                            quoteFound = false;
                        }
                        if (quoteFound && eName.equals("words")) {
                            wordsFound = false;
                        }
                        if (quoteFound && eName.equals("by")) {
                            byFound = false;
                        }
                    }


                    if (event.isCharacters()) {
                        Characters characters = (Characters) event;

                        if (byFound) {
                            by = characters.getData();

                            if (by.toLowerCase().contains(keyword.toLowerCase())) {
                                // 
                                System.out.println(words + " by " + by);
                            }
                        }
                        if (wordsFound) {
                            words = characters.getData();
                        }
                    }
                }
            }

        } finally {
            scanner.close();
        }
    }
}