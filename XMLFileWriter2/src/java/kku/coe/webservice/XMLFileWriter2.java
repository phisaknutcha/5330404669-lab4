
package kku.coe.webservice;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;

public class XMLFileWriter2 {
    public static void main(String argv[]) {

        File filename = new File("quotes.xml");

        try {

            XMLOutputFactory xof = XMLOutputFactory.newInstance();
            XMLStreamWriter xtw = xof.createXMLStreamWriter(new OutputStreamWriter(new FileOutputStream(filename)));

            
            xtw.writeStartDocument("UTF-8", "1.0");
            xtw.writeStartElement("quotes");
            xtw.writeStartElement("quote");
            xtw.writeStartElement("words");
            xtw.writeCharacters("Time is more value than money. You can get more money, but you cannot get more time");
            xtw.writeEndElement();//end element words
            xtw.writeStartElement("by");
            xtw.writeCharacters("Jim Rohn");
            xtw.writeEndElement();//end element by
            xtw.writeEndElement();//end element quote
            xtw.writeStartElement("quote");
            xtw.writeStartElement("words");
            xtw.writeCharacters("เมื่อทำอะไรสำเร็จ แม้จะเป็นก้าวเล็กๆของตัวเอง ก็ควรรู้จักให้รางวัลตัวเองบ้าง");
            xtw.writeEndElement();//end element words
            xtw.writeStartElement("by");
            xtw.writeCharacters("ว. วชิรเมธี");
            xtw.writeEndElement();//end element by
            xtw.writeEndElement();//end element quote
            xtw.writeEndElement();//end element quotes
            xtw.writeEndDocument();//end document
            xtw.flush();
            xtw.close();
            
            System.out.println("quote.xml was create successfull");
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}