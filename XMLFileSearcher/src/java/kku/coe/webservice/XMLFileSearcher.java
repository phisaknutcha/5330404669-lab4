package kku.coe.webservice;

import java.io.*;
import java.util.Scanner;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import org.w3c.dom.CharacterData;
import org.xml.sax.*;

public class XMLFileSearcher {

    public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException, ParserConfigurationException, SAXException, IOException {
        
        String keyword;
        
        String kwFile = "keyword.txt";
        File xmlfile = new File("quotes.xml");        
        Scanner scanner = new Scanner(new FileInputStream(kwFile), "UTF-8");
        
        try {
            while (scanner.hasNextLine()) {
                
                keyword = scanner.nextLine();
                
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = factory.newDocumentBuilder();
                Document doc = dBuilder.parse(xmlfile);
                NodeList quotes = doc.getElementsByTagName("quote");
                
                for (int i = 0; i < quotes.getLength(); i++) {
                    
                    Element item = (Element) quotes.item(i);
                    String bElement = getElemVal(item,"by").toLowerCase();
                    String kwLow = keyword.toLowerCase();
                    
                    if (bElement.contains(kwLow)) {
                        System.out.println(getElemVal(item, "words") + " by " + getElemVal(item, "by"));
                        
                    }
                }
            }
        } finally {
            scanner.close();
        }
    }

    protected static String getElemVal(Element parent, String lable) {
        Element e = (Element) parent.getElementsByTagName(lable).item(0);
        try {
            Node child = e.getFirstChild();
            if (child instanceof CharacterData) {
                CharacterData charData = (CharacterData) child;
                return charData.getData();
            }
        } catch (Exception ex) {
        }
        return "";
    }
}