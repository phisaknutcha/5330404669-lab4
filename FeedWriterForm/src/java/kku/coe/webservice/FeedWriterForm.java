/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package kku.coe.webservice;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;

/**
 *
 * @author mphisaknutcha
 */
@WebServlet(name = "FeedWriterFrom", urlPatterns = {"/FeedWriterFrom"})
public class FeedWriterForm extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    String filename = "C:/Users/mphisaknutcha/Documents/NetBeansProjects/FeedWriterForm/fileWriter.xml";
    File file = new File(filename);

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ParserConfigurationException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();

        String inputTitle = request.getParameter("title");
        String inputLink = request.getParameter("url");
        String inputDesc = request.getParameter("desc");

        try {
            if (!file.exists()) {
                DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder docBuilder = builderFactory.newDocumentBuilder();
                //creating a new instance of a DOM to build a DOM tree.
                Document doc = docBuilder.newDocument();
                FeedWriterForm fw = new FeedWriterForm();
                String feed = fw.createRssTree(doc, inputTitle, inputLink, inputDesc);
                out.print(feed);
            } else {
                DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder docBuilder = builderFactory.newDocumentBuilder();
                //creating a new instance of a DOM to build a DOM tree.
                Document doc = docBuilder.parse(file);
                FeedWriterForm fw = new FeedWriterForm();
                String feed = fw.updateRssTree(doc, inputTitle, inputLink, inputDesc);
                out.println(feed);
            }

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public String createRssTree(Document doc,String inputTitle, String inputLink, String inputDesc) throws Exception {
        //root element rss
        Element rss = doc.createElement("rss");

        //attribute version='2.0'in element rss
        rss.setAttribute("version", "2.0");
        doc.appendChild(rss);

        //the child element of rss is channel
        Element channel = doc.createElement("channel");
        rss.appendChild(channel);

        //Element/rss/channel/title
        Element title = doc.createElement("title");
        channel.appendChild(title);
        Text titleT = doc.createTextNode("Khon Kean University Rss Feed");
        title.appendChild(titleT);

        //element /rss/channel/description
        Element desc = doc.createElement("description");
        channel.appendChild(desc);
        Text descT = doc.createTextNode("Khon Kean University Information News Rss Feed");
        desc.appendChild(descT);

        //element/rss/channel/link
        Element link = doc.createElement("link");
        channel.appendChild(link);
        Text linkT = doc.createTextNode("http://www.kku.ac.th");
        link.appendChild(linkT);

        //element/rss/channel/lang
        Element lang = doc.createElement("lang");
        channel.appendChild(lang);
        Text langT = doc.createTextNode("en-th");
        lang.appendChild(langT);

        //element/rss/channel/item
        Element item = doc.createElement("item");
        channel.appendChild(item);

        //element/rss/channel/title
        Element iTitle = doc.createElement("title");
        item.appendChild(iTitle);
        Text iTitleT = doc.createTextNode(inputTitle);
        iTitle.appendChild(iTitleT);

        Element iDesc = doc.createElement("description");
        item.appendChild(iDesc);
        Text iDescT = doc.createTextNode(inputDesc);
        iDesc.appendChild(iDescT);

        Element iLink = doc.createElement("link");
        item.appendChild(iLink);
        Text iLinkT = doc.createTextNode(inputLink);
        iLink.appendChild(iLinkT);

        Element pubDate = doc.createElement("pubDate");
        item.appendChild(pubDate);
        Text pubDateT = doc.createTextNode((new java.util.Date()).toString());
        pubDate.appendChild(pubDateT);

        //Transformerfactory instance is used to create Transformer objects.
        TransformerFactory factory = TransformerFactory.newInstance();
        Transformer transformer = factory.newTransformer();

        transformer.setOutputProperty(OutputKeys.INDENT, "yes");

        //create string from xml tree
        StringWriter sw = new StringWriter();
        StreamResult result = new StreamResult(sw);
        DOMSource source = new DOMSource(doc);
        transformer.transform(source, result);
        String xmlString = sw.toString();

        //set to an appropriate file name in teh web project 
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)));
        bw.write(xmlString);
        bw.flush();
        bw.close();
        return xmlString;
    }

    public String updateRssTree(Document doc,String inputTitle, String inputLink, String inputDesc) throws Exception {
        Element channel = (Element) doc.getElementsByTagName("channel").item(0);

        Element item = doc.createElement("item");
        channel.appendChild(item);

        Element iTitle = doc.createElement("title");
        item.appendChild(iTitle);
        Text iTitleT = doc.createTextNode(inputTitle);
        iTitle.appendChild(iTitleT);

        Element iDesc = doc.createElement("description");
        item.appendChild(iDesc);
        Text iDescT = doc.createTextNode(inputDesc);
        iDesc.appendChild(iDescT);

        Element iLink = doc.createElement("link");
        item.appendChild(iLink);
        Text iLinkT = doc.createTextNode(inputLink);
        iLink.appendChild(iLinkT);

        Element pubDate = doc.createElement("pubDate");
        item.appendChild(pubDate);
        Text pubDateT = doc.createTextNode((new java.util.Date()).toString());
        pubDate.appendChild(pubDateT);

        //Transformerfactory instance is used to create Transformer objects.
        TransformerFactory factory = TransformerFactory.newInstance();
        Transformer transformer = factory.newTransformer();

        transformer.setOutputProperty(OutputKeys.INDENT, "yes");

        //create string from xml tree
        StringWriter sw = new StringWriter();
        StreamResult result = new StreamResult(sw);
        DOMSource source = new DOMSource(doc);
        transformer.transform(source, result);
        String xmlString = sw.toString();

        //set to an appropriate file name in teh web project 
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)));
        bw.write(xmlString);
        bw.flush();
        bw.close();
        return xmlString;

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(FeedWriterForm.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(FeedWriterForm.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public String getServletInfo() {
        return "Short	description";
    }//	</editor-fold>

}