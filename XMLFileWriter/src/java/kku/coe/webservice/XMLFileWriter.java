/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package kku.coe.webservice;


import java.io.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import org.w3c.dom.*;

public class XMLFileWriter {

    public static void main(String argv[]) {

        File file = new File("quotes.xml");

        try {

            DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = builderFactory.newDocumentBuilder();
            Document doc = docBuilder.newDocument();

            
            Element root = doc.createElement("quotes");
            doc.appendChild(root);

            
            Element quoteTag = doc.createElement("quote");
            root.appendChild(quoteTag);

            Element wordTag = doc.createElement("word");
            wordTag.appendChild(doc.createTextNode("Time is more value than money. You can get more money, but you cannot get more time"));
            quoteTag.appendChild(wordTag);

            Element byTag = doc.createElement("by");
            byTag.appendChild(doc.createTextNode("Jim Rohn"));
            quoteTag.appendChild(byTag);

            // Create second quote element 
            Element quoteTag2 = doc.createElement("quote");
            root.appendChild(quoteTag2);

            Element wordTag2 = doc.createElement("word");
            wordTag2.appendChild(doc.createTextNode("เมื่อทำอะไรสำเร็จ แม้จะเป็นเก้าเล้กๆของตัวเอง ก็ควรรู้จักให้รางวัลตัวเองบ้าง"));
            quoteTag2.appendChild(wordTag2);

            Element byTag2 = doc.createElement("by");
            byTag2.appendChild(doc.createTextNode("ว. วชิรเมธี"));
            quoteTag2.appendChild(byTag2);

            TransformerFactory transform = TransformerFactory.newInstance();
            Transformer trans = transform.newTransformer();
            trans.setOutputProperty(OutputKeys.INDENT, "yes");
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(file);

            trans.transform(source, result);

            System.out.println("Create quotes.xml Success!!!");

        } catch (Exception e) {
            System.out.println(e);
        }
    }
}