package kku.coe.webservice;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.net.*;
import javax.xml.stream.*;
import javax.xml.stream.events.*;
import java.io.*;
import javax.servlet.annotation.WebServlet;

@WebServlet(name = "FeedSearcher2", urlPatterns = {"/FeedSearcher2"})
public class FeedSearcher2 extends HttpServlet {

    XMLEventReader reader;

        protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        
        PrintWriter out = response.getWriter();
        String input = request.getParameter("url");
        String keyword = request.getParameter("keyword");
        
        boolean titleFound = false;
        boolean linkFound = false;
        boolean itemFound = false;
        boolean checkKW = false;
        
        String link = null;
        String eName;
        try {
            URL u = new URL(input);
            InputStream in = u.openStream();
            XMLInputFactory factory = XMLInputFactory.newInstance();
            reader = factory.createXMLEventReader(in);
            out.print("<html><body><table border ='1'><tr><th>Title</th ><th>Link</th></tr>");
            while (reader.hasNext()) {
                XMLEvent event = reader.nextEvent();
                if (event.isStartElement()) {
                    StartElement element = (StartElement) event;
                    eName = element.getName().getLocalPart();
                    if (eName.equals("item")) {
                        itemFound = true;
                    }
                    if (itemFound && eName.equals("link")) {
                        linkFound = true;
                       
                    }
                    if (itemFound && eName.equals("title")) {
                        titleFound = true;
                       
                    }
                }
                if (event.isEndElement()) {
                    EndElement element = (EndElement) event;
                    eName = element.getName().getLocalPart();
                    if (eName.equals("item")) {
                        itemFound = false;
                    }
                    if (itemFound && eName.equals("link")) {
                        
                        linkFound = false;
                    }
                    if (itemFound && eName.equals("title")) {
                        
                        titleFound = false;
                    }
                }
                if (event.isCharacters()) {
                    Characters characters = (Characters) event;
                    if (linkFound) {
                        if(checkKW){
                        out.print("<td>");
                        out.print(characters.getData());
                        out.print("</td></tr>");
                        checkKW = false;
                        }
                    }

                    if (titleFound) {
                        if(characters.getData().toLowerCase().contains(keyword.toLowerCase()))
                        {
                        checkKW = true;
                        out.print("<tr><td>");
                        out.print(characters.getData());
                        out.print("</td>");
                        }
                    }
                }
            }			//	end	while
            reader.close();
            out.print("</table></body></html>");
        } catch (Exception ex) {
            ex.printStackTrace(System.err);
        } finally {
            out.close();
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short	description";
    }
}