package kku.coe.webservice;

import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import javax.xml.stream.*;
import javax.xml.stream.events.*;

@WebServlet(name = "FeedWriterForm2", urlPatterns = {"/FeedWriterForm2"})
public class FeedWriterForm2 extends HttpServlet {

    // filePath is a location of feed file
    String filename = "D:/feed2k.xml";
    File file = new File(filename);

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // Set encoding of request data is UTF-8
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();


        String inputTitle = request.getParameter("title");
        String inputLink = request.getParameter("url");
        String inputDesc = request.getParameter("desc");
        
        try {


            if (file.exists()) {
                // If the feed file exits, the lastest filled information
                // is appended to the existing file
                new FeedWriterForm2().updateRssFeed(inputTitle,inputLink,inputDesc);
            } else {
                // If the feed file does not exits, the programe creates
                // a new feed file
                new FeedWriterForm2().createRssFeed(inputTitle,inputLink,inputDesc);
            }

            out.println("<b <a href='http://localhost:8080/FeedWriterForm2/feed2.xml'>" + "Rss Feed</a> was create successfully</b>");

        } catch (Exception e) {
            System.out.println();
        }
    }

    private void createRssFeed(String inputTitle,String inputDesc,String inputLink) throws Exception {
        
        XMLOutputFactory xof = XMLOutputFactory.newInstance();
        XMLStreamWriter xtw = xof.createXMLStreamWriter(new OutputStreamWriter(
                new FileOutputStream(file)));

        // Write XML prologue
        xtw.writeStartDocument();
        xtw.writeStartElement("rss");
        xtw.writeAttribute("version", "2.0");
        xtw.writeStartElement("channel");
        xtw.writeStartElement("title");
        xtw.writeCharacters("Khon Kean University RSS Feed");
        xtw.writeEndElement();
        //end title
        xtw.writeStartElement("description");
        xtw.writeCharacters("Khon Kean University Information News RSS Feed");
        xtw.writeEndElement();
        //end description
        xtw.writeStartElement("link");
        xtw.writeCharacters("http://www.kku.ac.th");
        xtw.writeEndElement();
        //end link
        xtw.writeStartElement("lang");
        xtw.writeCharacters("en~th");
        xtw.writeEndElement();
        //end lang

        //element/rss/channel/item
        xtw.writeStartElement("item");
        
        //element/rss/channel/item/title
        xtw.writeStartElement("title");
        xtw.writeCharacters(inputTitle);
        xtw.writeEndElement();
        
        //element/rss/channel/item/description
        xtw.writeStartElement("description");
        xtw.writeCharacters(inputDesc);
        xtw.writeEndElement();
        
        //element/rss/channel/item/link
        xtw.writeStartElement("link");
        xtw.writeCharacters(inputLink);
        xtw.writeEndElement();
        
        //element/rss/channel/item/pubDate
        xtw.writeStartElement("pubDate");
        xtw.writeCharacters((new java.util.Date()).toString());
        xtw.writeEndElement();
        xtw.writeEndElement();
        
        //end element item
        xtw.writeEndElement();
        //end element channel
        xtw.writeEndElement();
        //end element rss
        xtw.writeEndDocument();
        xtw.flush();
        xtw.close();
    }

    public void updateRssFeed(String inputTitle,String inputDesc,String inputLink) throws Exception {
        
        XMLInputFactory xif = XMLInputFactory.newInstance();
        XMLEventReader reader = xif.createXMLEventReader(new InputStreamReader(new FileInputStream(file)));
        XMLOutputFactory xof = XMLOutputFactory.newInstance();
        XMLStreamWriter writer = xof.createXMLStreamWriter(new OutputStreamWriter(new FileOutputStream(file)));
        String eName;

        
        while (reader.hasNext()) {
            XMLEvent event = reader.nextEvent();
            if (event.isStartDocument()) {
                writer.writeStartDocument();
            }

            if (event.isStartElement()) {
                StartElement element = (StartElement) event;
                eName = element.getName().getLocalPart();
                if (eName.equals("rss")) {
                    writer.writeStartElement("rss");
                    writer.writeAttribute("version", "2.0");
                } else {
                    writer.writeStartElement(eName);
                }
            }

            
            if (event.isEndElement()) {
                EndElement element = (EndElement) event;
                eName = element.getName().getLocalPart();
                
                if (eName.equals("channel")) {
                    //element/rss/channel/item
                    writer.writeStartElement("item");
                    
                    //element/rss/channel/item/title
                    writer.writeStartElement("title");
                    writer.writeCharacters(inputTitle);
                    writer.writeEndElement();
                    
                    //element/rss/channel/item/description
                    writer.writeStartElement("description");
                    writer.writeCharacters(inputDesc);
                    writer.writeEndElement();
                    
                    //element/rss/channel/item/link
                    writer.writeStartElement("link");
                    writer.writeCharacters(inputLink);
                    writer.writeEndElement();
                    
                    //element/rss/channel/item/pubDate
                    writer.writeStartElement("pubDate");
                    writer.writeCharacters((new java.util.Date()).toString());
                    writer.writeEndElement();
                    writer.writeEndElement();
                    writer.writeEndDocument();
                    writer.flush();
                    writer.close();
                    return;
                } else {
                    writer.writeEndElement();
                }
            }

            // If this part of data is characters section
            if (event.isCharacters()) {
                Characters characters = (Characters) event;
                String c = characters.getData().toString();
                writer.writeCharacters(c);
            }
        }

        writer.flush();
        writer.close();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}